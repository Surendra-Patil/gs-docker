# Gstudio Service Docker

## Usage
1.  (Optional) Create a docker bridge network  "docker network create gstudio-network"  and uncomment the last 4 lines from "docker-compose.yml" file.
2.  Git clone this repository.
3.  Go inside the repository "cd gs-docker"
4.  Run command "docker-compose up"

## Image will be created and container will be started.
